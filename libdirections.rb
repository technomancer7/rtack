$directions = ["north", "east", "south", "west", "up", "down", "in", "out"]
$directionsall = ["north", "east", "south", "west", "up", "down", "in", "out", "n", "e", "w", "s", "u", "d", "i", "o"]
$directionsshort= ["n", "e", "w", "s", "u", "d", "i", "o"]
$directionscardinal = ["north", "east", "south", "west"]

module Directions
  def Directions.validate(s)
    case s
    when "north", "n"
      return "north"
    when "south", "s"
      return "south"
    when "west", "w"
      return "west"
    when "east", "e"
      return "east"
    when "up", "u"
      return "up"
    when "down", "d"
      return "down"
    when "in", "i"
      return "in"
    when "out", "o"
      return "out"
    else
      return ""
    end    
  end
  def Directions.invert(s)
     case s
        when "north"
            return "south"
        when "south"
            return "north"
        when "west"
            return "east"
        when "east"
            return "west"
        when "up"
            return "down"
        when "down"
            return "up"
        when "in"
            return "out"
        when "out"
            return "in"
        when "n"
            return "s"
        when "s"
            return "n"
        when "w"
            return "e"
        when "e"
            return "w"
        when "u"
            return "d"
        when "d"
            return "u"
        when "i"
            return "o"
        when "o"
            return "i"
        else
          return s
     end
               
  end
  def Directions.shorten(s)
    case s
    when "north"
      return "n"
    when "south"
      return "s"
    when "west"
      return "w"
    when "east"
      return "e"
    when "up"
      return "u"
    when "down"
      return "d"
    when "in"
      return "i"
    when "out"
      return "o"
    else
      return s
    end
  end
  def Directions.expand(s)
    case s
    when "n"
      return "north"
    when "s"
      return "south"
    when "w"
      return "west"
    when "e"
      return "east"
    when "u"
      return "up"
    when "d"
      return "down"
    when "i"
      return "in"
    when "o"
      return "out"
    else
      return s
    end
  end

end
