# Wearables, adds wear/remove commands, exposes worn/removed events, requires wearable tag in item

class Worldbuilder
  def loadWearables
    self.registerCommand(:wear) do |state, msg|
      state.txt "wearable", "Trying to wear item..."
    end
  end
end
