# mapping extension (copy from the python one), adds map command, use the moved_x events to handle navigation and layers

require_relative 'mapping'
class Worldbuilder
  def loadMap
    @mapper = Mapper.new
    self.registerCommand(:map) do |state, msg|
      state.txt "map", "<Iterate over map>"
    end
  end
end
